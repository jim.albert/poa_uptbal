<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PoaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idpoa') ?>

    <?= $form->field($model, 'año') ?>

    <?= $form->field($model, 'lineamiento') ?>

    <?= $form->field($model, 'objetivo_esrategico') ?>

    <?= $form->field($model, 'id_area') ?>

    <?php // echo $form->field($model, 'objetivo_parroquial') ?>

    <?php // echo $form->field($model, 'proyecto') ?>

    <?php // echo $form->field($model, 'objetivo_proyecto') ?>

    <?php // echo $form->field($model, 'id_unidad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
