<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


use app\models\Areaaccionunidadesponsable;
/* @var $this yii\web\View */
/* @var $model app\models\Poa */
/* @var $form yii\widgets\ActiveForm */
$jsc = <<< JS

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
JS;

$this->registerJs($jsc, $this::POS_END);


?>

<div class="poa-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= Html::activeLabel($model, 'id_unidad', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="Ésta representa la Unidad, Oficina o Dirección donde usted labora.">
    ?
    </span>
    <?=  $form->field($model, 'id_unidad')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Areaaccionunidadesponsable::find()
        ->where(['id_usuario' => Yii::$app->user->identity->id])
        ->all(), 'idareaaccionunidadesponsable','unidadnombre','areaccionnombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        'title' => 'Selecione una opción ...',


        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label(false);
    ?>

    <?= Html::activeLabel($model, 'ano', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="AÑO EN EL QUE SE DESARROLLARÁ LA PLANIFICACIÓN ESTRATÉGICA INSTITUCIONAL.">
    ?
    </span>
    <?= $form->field($model, 'ano')->textInput(['maxlength' => true])->label(false) ?>

    <?= Html::activeLabel($model, 'lineamiento', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="LINEAMIENTO ESTRATÉGICO SEGUNDO PLAN SOCIALISTA DE DESARROLLO ECONÓMICO Y SOCIAL DE LA NACIÓN 2013 - 2019.">
    ?
    </span>
    <?= $form->field($model, 'lineamiento')->textarea(['rows' => 6])->label(false) ?>

    <?= Html::activeLabel($model, 'objetivo_esrategico', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="SON LOS OBJETIVOS DISEÑADOS Y TRAZADOS POR EL DISTRITO MOTOR DE DESARROLLO EN LOS QUE SU DEPENDENCIA EJERCERÁ IMPACTO.">
    ?
    </span>
    <?= $form->field($model, 'objetivo_esrategico')->textarea(['rows' => 6])->label(false) ?>

    <?= Html::activeLabel($model, 'proyecto', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="ES UNA VISIÓN GENERAL, CON UNIDADES DE MEDIDAS DEL IMPACTO QUE OCACIONARÁ LA DEPENDENCIA.">
    ?
    </span>
    <?= $form->field($model, 'proyecto')->textarea(['rows' => 6])->label(false) ?>


    <?= Html::activeLabel($model, 'objetivo_proyecto', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right" title="SON LAS ACCIONES GENERALES QUE CONTRIBUYEN AL LOGRO DE LAS METAS DEL PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO DE LA UPTBAL.">
    ?
    </span><?= $form->field($model, 'objetivo_proyecto')->textarea(['rows' => 6])->label(false) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
