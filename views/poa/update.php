<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poa */

$this->title = 'Actualizar Plan Integral de Desarrollo: ' . $model->idpoa;
if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $this->params['breadcrumbs'][] = ['label' => 'Planes Operativos', 'url' => ['index2']];

}else {
  $this->params['breadcrumbs'][] = ['label' => 'Planes Operativos', 'url' => ['index']];

}
$this->params['breadcrumbs'][] = ['label' => $model->idpoa, 'url' => ['view', 'id' => $model->idpoa]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="poa-update">

     <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
