<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


use app\models\Areaccion;
use app\models\Unidadresponsable;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PoaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plan Integral de Desarrollo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poa-index">

     <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Poa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idpoa',

            //'lineamiento:ntext',
            //'objetivo_esrategico:ntext',
            //'id_area',
            /*[
             'attribute' => 'id_area',
                'value'     => 'idArea.descripcion',
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_area',
                                'data' => ArrayHelper::map(Areaccion::find()->orderBy('descripcion ASC')->all(), 'idareaccion','descripcion'),

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],*/
            // 'objetivo_parroquial:ntext',
            // 'proyecto:ntext',
            // 'objetivo_proyecto',
             //'id_unidad',
             [
             'attribute' => 'id_unidad',
                'value'     => 'idUnidadDesc',
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_unidad',
                                'data' => ArrayHelper::map(Unidadresponsable::find()->orderBy('descripcion ASC')->all(), 'idunidadresponsble','descripcion'),

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],
                'ano',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
