<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\models\Poa;
use app\models\Dtpoa;
use app\models\Avance;



$Poa=Poa::find()->where(['idpoa' => $poa])->one();



$this->title = 'SISTEMA DE PLANIFICACION - PLAN OPERATIVO '.strtoupper($Poa->unidadnombre);
//echo $municipio;


echo Html::img('@web/images/cintillo.png', ['alt' => Html::encode($this->title),'title' => Html::encode($this->title), 'width' => '2000']);


?>

<div class="listado-parroquia">

		<div >
              <h3 class="modal-header-danger">
                     PLAN OPERATIVO
              </br>
                    <?= strtoupper($Poa->unidadnombre) ?>
                                                
              </h3>
		</div>



<table class="table table-striped table-bordered">
  
    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'ano'); ?></strong></td>
      <td> <?= $Poa->ano ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'lineamiento'); ?></strong></td>
      <td> <?= $Poa->lineamiento ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'objetivo_esrategico'); ?></strong></td>
      <td> <?= $Poa->objetivo_esrategico ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'id_area'); ?></strong></td>
      <td> <?= $Poa->idArea->descripcion ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'objetivo_parroquial'); ?></strong></td>
      <td> <?= $Poa->objetivo_parroquial ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'proyecto'); ?></strong></td>
      <td> <?= $Poa->proyecto ?> </td>
    </tr>
    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'objetivo_proyecto'); ?></strong></td>
      <td> <?= $Poa->objetivo_proyecto ?> </td>
    </tr>


</table>


<table class=" table-striped table-bordered">
  
    <tr class="bg bg-danger">
      <td align="center"  rowspan="3"><strong>N°</strong></td>
      <td align="center"  rowspan="3"><strong>Acciones</strong></td>
      <td align="center"  rowspan="3"><strong>Unidad de Medida </strong></td>
      <td align="center"  rowspan="3"><strong>Meta Anual</strong></td>

      <td align="center"  colspan="12"><strong>Programación Física</strong></td>

      
      
    </tr>
    <tr class="bg bg-warning">
    <td align="center"  colspan="3"><strong>TRIMESTRE I</strong></td>
    <td align="center"  colspan="3"><strong>TRIMESTRE II</strong></td>
    <td align="center"  colspan="3"><strong>TRIMESTRE III</strong></td>
    <td align="center"  colspan="3"><strong>TRIMESTRE IV</strong></td>
  </tr>

  <tr  class="bg bg-success">
    <td align="center"><strong>ENE</strong></td>
    <td align="center"><strong>FEB</strong></td>
    <td align="center"><strong>MAR</strong></td>
    <td align="center"><strong>ABR</strong></td>
    <td align="center"><strong>MAY</strong></td>
    <td align="center"><strong>JUN</strong></td>
    <td align="center"><strong>JUL</strong></td>
    <td align="center"><strong>AGO</strong></td>
    <td align="center"><strong>SEP</strong></td>
    <td align="center"><strong>OCT</strong></td>
    <td align="center"><strong>NOV</strong></td>
    <td align="center"><strong>DIC</strong></td>
  </tr>
    
    <?php $Dtpoa=Dtpoa::find()->where(['id_poa' => $poa])->all();   

      $Avance=0;
      $efectividad=0;
      $meta_anual=0;
      foreach($Dtpoa as $key => $value) {

        $meta_anual=$value->meta_anual;
        echo "<tr>";
            echo "<td><strong>". strtoupper($key) ."</strong></td>";
            echo "<td>". strtoupper($value->actividad) ."</td>";
            echo "<td align=center>". strtoupper($value->idUnidadMedida->descripcion) ."</td>";
            echo "<td align=center>". number_format($meta_anual, 2, ",", ".") ."</td>"; 

           //$Avancet1=Avance::find()->where(['between','mes', "1","3"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            echo "<td align=center>". number_format($value->enero, 0, ",", ".") ."</td>"; 

            echo "<td align=center>". number_format($value->febrero, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->marzo, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->abril, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->mayo, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->junio, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->julio, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->agosto, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->setiembre, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->octubre, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->noviembre, 0, ",", ".") ."</td>";
            echo "<td align=center>". number_format($value->diciembre, 0, ",", ".") ."</td>";
            

        echo "</tr>";
      }

    ?>

      



    </tr>

   
    

</table>
		
</div>

