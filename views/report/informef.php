<?php
ini_set("memory_limit","1024M");
set_time_limit(4200);
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\chartjs\ChartJs;
use app\models\Poa;
use app\models\Areaccion;
use app\models\Dtpoa;
use app\models\Avance;


echo  Html::img('@web/images/uptbal.gif', ['alt' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” ','title' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” ']);

//$Poa=Poa::find()->where(['idpoa' => $poa])->one();
$this->title = 'PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO DE LA UPTBAL';


$Poa=Poa::find()->where(['id_unidad'=>$id])->all();
foreach($Poa as $key => $value) {


?>

<div class="listado-parroquia">

		<div >
              <h3 class="modal-header-danger">
                    PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO -
              </br>
                    <?= strtoupper($value->idUnidadDesc) ?>

              </h3>
		</div>



<table class="table table-striped table-bordered">

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'ano'); ?></strong></td>
      <td> <?= $value->ano ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'lineamiento'); ?></strong></td>
      <td> <?= nl2br(strtoupper($value->lineamiento)) ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_esrategico'); ?></strong></td>
      <td> <?= nl2br(strtoupper($value->objetivo_esrategico)) ?> </td>
    </tr>


    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'proyecto'); ?></strong></td>
      <td> <?= nl2br(strtoupper($value->proyecto)) ?> </td>
    </tr>
    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_proyecto'); ?></strong></td>
      <td> <?= nl2br(strtoupper($value->objetivo_proyecto)) ?> </td>
    </tr>


		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.definicion'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->definicion)) ?> </td>
		</tr>
		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.vision'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->vision)) ?> </td>
		</tr>
		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.mision'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->mision)) ?> </td>
		</tr>
		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.objetivo_general'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->objetivo_general)) ?> </td>
		</tr>
		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.objetivos_especificos'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->objetivos_especificos)) ?> </td>
		</tr>

		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.funciones'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->funciones)) ?> </td>
		</tr>
		<tr>
			<td align="center"><strong><?= Html::activeLabel($value, 'idUnidad.unidadresponsble.lineas_estrategicas'); ?></strong></td>
			<td> <?= nl2br(strtoupper($value->idUnidad->unidadresponsble->lineas_estrategicas)) ?> </td>
		</tr>




</table>


<table class=" table-bordered">





    <?php $Dtpoa=Dtpoa::find()->where(['id_poa' => $value->idpoa, ])->all();

      $Avance=0;
      $efectividad=0;
      $meta_anual=0;
      foreach($Dtpoa as $key => $value) {

        $meta_anual=$value->meta_anual;

				$Avancet1=Avance::find()->where(['between','mes', "1","3"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

										$Avancet2=Avance::find()->where(['between','mes', "4","6"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

										$Avancet3=Avance::find()->where(['between','mes', "7","9"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

										$Avancet4=Avance::find()->where(['between','mes', "10","12"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

										$Avance=$Avancet1+$Avancet2+$Avancet3+$Avancet4;

										$efectividad1=$Avance/$meta_anual*100;

										/*if ($efectividad>100) {
													//$mtatem=mt_rand(3, 9);
													$meta_anual2=$Avance/4*4.5;

													if ($meta_anual2>0){
														$efectividad1=$Avance/$meta_anual2*100;
														$meta_anual=$meta_anual2;
													}else {
														$efectividad1=0;
													}
										} else {
											$efectividad1=$efectividad;
										}*/
?>

		      <tr >

						<td align="center"  class="bg bg-info"><strong>N°</strong></td>
						<td> <?=strtoupper($key)?></td>
						<td rowspan="10" align="center" > <div class="label label-success">

							<?=strtoupper($value->idUnidadMedida->descripcion)?>
							</div>
<?php
							$etiquetas1=['I','II','III','IV'];
			$datos1=[$Avancet1,$Avancet2,$Avancet3,$Avancet4];
			$r1=mt_rand(0, 255);
			$g1=mt_rand(0, 255);
			$b1=mt_rand(0, 255);
			$Color[] = "rgba(".$r1.",".$g1.",".$b1.",0.5)";
			$bColor[] = "rgba(".$r1.",".$g1.",".$b1.",2)";
			$tip=mt_rand(1, 3);
			$values=[
					'1'=>'pie',
					'2'=>'doughnut',
					'3'=>'bar',
			];
			?>

			<?= ChartJs::widget([
				'type' => $values[$tip],//'pie',
				'options' => [
						//  'id' => 'cstat',

							//'responsive' => true,
							//'animation'=> true,
			],
				'clientOptions' => [
						'legend' => ['display' => true],
						'tooltips' => ['enabled' => true],
				],
				'data' => [
						'labels' => $etiquetas1,
						'datasets' => [
								[
									'backgroundColor' => $Color,
									'borderColor' => $bColor,
									'pointBackgroundColor' => $bColor,
									'pointBorderColor' => "#fff",
									'pointHoverBackgroundColor' => "#fff",
									'pointHoverBorderColor' => $bColor,
										'data' => $datos1,
								],

						],
				]
		]);
		?>



						</td>
					</tr >
		      <tr >

						<td align="center" class="bg bg-info"><strong>Acciones</strong></td>
						<td> <?=strtoupper($value->actividad)?></td>

					</tr >
		      <tr >

						<td align="center"  class="bg bg-info"><strong>Unidad de Medida </strong></td>
						<td> <?=strtoupper($value->idUnidadMedida->descripcion)?></td>

					</tr >
		      <tr>

						<td align="center" class="bg bg-info" ><strong>Meta Anual</strong></td>
						<td> <?=number_format($meta_anual, 0, ",", ".")?></td>

					</tr >


		    <tr >

					<td align="center" class="bg bg-success"><strong>Trimestre I</strong></td>
					<td> <?=number_format($Avancet1, 0, ",", ".")?></td>

				</tr >
		    <tr >

					<td align="center" class="bg bg-success"><strong>Trimestre II</strong></td>
					<td> <?=number_format($Avancet2, 0, ",", ".")?></td>

				</tr >
		    <tr >

					<td align="center" class="bg bg-success"><strong>Trimestre III</strong></td>
					<td> <?=number_format($Avancet3, 0, ",", ".")?></td>

				</tr >
		    <tr >

					<td align="center" class="bg bg-success"><strong>Trimestre IV</strong></td>
					<td><?=number_format($Avancet4, 0, ",", ".")?></td>

				</tr >
				<tr >

					<td align="center" class="bg bg-info"><strong>Total</strong></td>
					<td> <?=number_format($Avance, 0, ",", ".")?></td>

				</tr >
				<tr >

					<td align="center" class="bg bg-warning"  ><strong>Efectividad</strong></td>
					<td> <?=number_format($efectividad1, 2, ",", ".")?>%</td>



				</tr >
				<tr >
					<td colspan="3" class="bg bg-danger"  >&nbsp;</td>
				</tr >






<?php



      }

    ?>


</table>

</div>

<?php } ?>
