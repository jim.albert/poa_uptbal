<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Estado;
use app\models\Municipio;
use app\models\Parroquia;
/* @var $this yii\web\View */
/* @var $model app\models\Comunidad */
/* @var $form yii\widgets\ActiveForm */

$jsc = <<< JS



function pdf(){    

    var estado = $('select#comunidad-estado').val();
    var municipio = $('select#comunidad-municipio').val();
    var parroquia = $('select#comunidad-parroquia').val();
    var comunidad = $('select#calle-id_comunidad').val();

    //alert(municipio);
    if(estado!=""){
         window.open("index.php?r=report/callespdf&estado="+estado+"&municipio="+municipio+"&parroquia="+parroquia+"&comunidad="+comunidad);
    }else{
        alert("Debe Selecionar al menos un estado para poder realizar la consulta");
    }
   

   
    
}
        
JS;

$this->registerJs($jsc, $this::POS_END);

$this->title = 'SISTEMA POPULAR DE DISTRIBUCION DE ALIMENTOS - Listado de Calles';

?>

<div class="comunidad-form">

    <h3 class="modal-header-danger">Listado de Calles</h3>

    <?php $form = ActiveForm::begin(); ?>

<div class="col-md-6">
    
    <?=  $form->field($comunidad, 'estado')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Estado::find()->orderBy('nombre ASC')->all(), 'id','nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',
        'onchange'  => '
                $.post("index.php?r=municipio/get-municipios&id=' . '" + $(this).val(), function(data){
                    $("select#comunidad-municipio").html(data);
                })
            ',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    </div>
<div class="col-md-6">
    <?=  $form->field($comunidad, 'municipio')->widget(Select2::classname(), [
       // 'data' => ArrayHelper::map(Estado::find()->orderBy('nombre ASC')->all(), 'id','nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',
        'onchange'  => '
                $.post("index.php?r=parroquia/get-parroquias&id=' . '" + $(this).val(), function(data){
                    $("select#comunidad-parroquia").html(data);
                })',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

</div>
<div class="col-md-6">
    <?= $form->field($comunidad, 'parroquia')->widget(Select2::classname(), [
       // 'data' => [2 => 'Music'], // ensure at least the preselected value is available
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',
        'onchange'  => '
                $.post("index.php?r=comunidad/get-comunidad&id=' . '" + $(this).val(), function(data){
                    $("select#calle-id_comunidad").html(data);
                })',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]);
    ?>
</div>
<div class="col-md-6">
    <?= $form->field($calle, 'id_comunidad')->widget(Select2::classname(), [
       // 'data' => [2 => 'Music'], // ensure at least the preselected value is available
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]);
    ?>

</div>
<div class="danger" align="center">
    
    <?= Html::a('<span class="glyphicon glyphicon-print"></span>',null, ['target'=>'_blank','class' => 'btn btn-danger','onClick'  =>'pdf();',]) ?>

   
    <?= Html::a('<span class="glyphicon glyphicon-export"></span>',null, ['target'=>'_blank','class' => 'btn btn-success','onClick'  =>'pdf();',]) ?>

    <?= Html::a('<span class="glyphicon glyphicon-home"></span>', ['site/index'], ['class' => 'btn btn-default',]) ?>
</div>

    <?php ActiveForm::end(); ?>

</div>
