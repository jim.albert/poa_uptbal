<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Poa;
use app\models\Dtpoa;
/* @var $this yii\web\View */
/* @var $model app\models\Avance */
/* @var $form yii\widgets\ActiveForm */


$jsc = <<< JS



function pdf(){

    var poa = $('select#avance-poa').val();


    //alert(municipio);
    if(poa!=""){
         window.open("index.php?r=report/poapdf&poa="+poa);
    }else{
        alert("Debe Selecionar al menos Plan Operativo para imprimir el avance");
    }




}

JS;

$this->registerJs($jsc, $this::POS_END);

$this->title = 'SISTEMA DE PLANIFICACION -  PLAN OPERATIVO';

if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $data=ArrayHelper::map(Poa::find()->joinWith('idUnidad')
  ->where(['id_usuario' => Yii::$app->user->identity->id])->orderBy('id_unidad ASC')->all(), 'idpoa','ano');
}else {
  $data=ArrayHelper::map(Poa::find()->orderBy('id_unidad ASC')->all(), 'idpoa','ano','idUnidadDesc');
}

?>

<div class="avance-form">

    <?php $form = ActiveForm::begin(); ?>

    <h3 class="modal-header-danger">Plan Operativos</h3>

    <?=  $form->field($model, 'poa')->widget(Select2::classname(), [
        'data' => $data,
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',
        /*'onchange'  => '
                $.post("index.php?r=dtpoa/get-actividades&id=' . '" + $(this).val(), function(data){
                    $("select#avance-id_dtpoa").html(data);
                })
            ',*/
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>




    <div class="danger" align="center">

        <?= Html::a('<span class="glyphicon glyphicon-print"></span>',null, ['target'=>'_blank','class' => 'btn btn-danger','onClick'  =>'pdf();',]) ?>


    <?= Html::a('<span class="glyphicon glyphicon-export"></span>',null, ['target'=>'_blank','class' => 'btn btn-success','onClick'  =>'pdf();',]) ?>

    <?= Html::a('<span class="glyphicon glyphicon-home"></span>', ['site/index'], ['class' => 'btn btn-default',]) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
