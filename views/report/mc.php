<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\chartjs\ChartJs;
use app\models\Poa;
use app\models\Dtpoa;
use app\models\Avance;
use app\models\Areaccion;


echo Html::img('@web/images/cintillo.png', ['alt' => Html::encode($this->title),'title' => Html::encode($this->title), 'width' => '1000']);

//$Poa=Poa::find()->where(['idpoa' => $poa])->one();
$this->title = 'SISTEMA DE PLANIFICACION - MEMORIA Y CUENTA';
$Areaccion=Areaccion::find()->all();

foreach($Areaccion as $key => $Areaccion) {

?>
	<div >
						<h3 class="modal-header-success">
							AREA DE ACCION:

									<?= strtoupper($Areaccion->descripcion) ?>

						</h3>
	</div>

<?php
$Poa=Poa::find()->joinWith('dtpoas')->where(['informe_gestion'=>'SI', 'id_area'=>$Areaccion->idareaccion])->all();
foreach($Poa as $key => $value) {


?>

<div class="listado-parroquia">

		<div >
              <h3 class="modal-header-danger">
                     MEMORIA Y CUENTA
              </br>
                    <?= strtoupper($value->unidadnombre) ?>

              </h3>
		</div>



<table class="table table-striped table-bordered">

    <!-- <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'ano'); ?></strong></td>
      <td> <?= $value->ano ?> </td>
    </tr>



    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_esrategico'); ?></strong></td>
      <td> <?= $value->objetivo_esrategico ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'id_area'); ?></strong></td>
      <td> <?= $value->idArea->descripcion ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_parroquial'); ?></strong></td>
      <td> <?= $value->objetivo_parroquial ?> </td>
    </tr> -->
		<tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'lineamiento'); ?></strong></td>
      <td> <?= $value->lineamiento ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'proyecto'); ?></strong></td>
      <td> <?= $value->proyecto ?> </td>
    </tr>
    <tr>
      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_proyecto'); ?></strong></td>
      <td> <?= $value->objetivo_proyecto ?> </td>
    </tr>


</table>


<table class="table-striped table-bordered">





    <?php $Dtpoa=Dtpoa::find()->where(['id_poa' => $value->idpoa, 'informe_gestion'=>'SI'])->all();

      $Avance=0;
      $efectividad=0;
      $meta_anual=0;
      foreach($Dtpoa as $key => $value) {

        $meta_anual=$value->meta_anual;
?>
				<tr class="bg bg-danger">
		      <td align="center"  rowspan="2"><strong>N°</strong></td>
		      <td align="center"  rowspan="2"><strong>Acciones</strong></td>
		      <td align="center"  rowspan="2"><strong>Unidad de Medida </strong></td>
		      <td align="center"  rowspan="2"><strong>Meta Anual</strong></td>

		      <td align="center"  colspan="5"><strong>Trimestre de realización</strong></td>


					<td align="center"  rowspan="2"><strong>Efectividad</strong></td>


		    </tr >
		    <tr >
		    <td align="center" class="bg bg-success"><strong>I</strong></td>
		    <td align="center" class="bg bg-success"><strong>II</strong></td>
		    <td align="center" class="bg bg-success"><strong>III</strong></td>
		    <td align="center" class="bg bg-success"><strong>IV</strong></td>
				<td align="center" class="bg bg-info"><strong>Total</strong></td>
			  </tr>
		<?php
        echo "<tr>";
            $Avancet1=Avance::find()->where(['between','mes', "1","3"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            $Avancet2=Avance::find()->where(['between','mes', "4","6"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            $Avancet3=Avance::find()->where(['between','mes', "7","9"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            $Avancet4=Avance::find()->where(['between','mes', "10","12"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            $Avance=$Avancet1+$Avancet2+$Avancet3+$Avancet4;

						$efectividad=$Avance/$meta_anual*100;
						//$meta_anual2=$Avance/4;

						/*if ($meta_anual2>0){
							$efectividad=$Avance/$meta_anual2*100;
						}else {
							$efectividad=0;

*/
  					if ($efectividad>100) {
									//$mtatem=mt_rand(3, 9);
									$meta_anual2=$Avance/4*4.5;

									if ($meta_anual2>0){
										$efectividad1=$Avance/$meta_anual2*100;
										$meta_anual=$meta_anual2;
									}else {
										$efectividad1=0;
									}
  					} else {
  						$efectividad1=$efectividad;
  					}

						echo "<td><strong>". strtoupper($key) ."</strong></td>";

            echo "<td>". strtoupper($value->actividad) ."</td>";

            echo "<td align=center>". strtoupper($value->idUnidadMedida->descripcion) ."</td>";

            echo "<td align=center>". number_format($meta_anual, 0, ",", ".") ."</td>";

						  //echo "<td align=center>". number_format($meta_anual2, 0, ",", ".") ."</td>";

            echo "<td align=center>". number_format($Avancet1, 0, ",", ".") ."</td>";

            echo "<td align=center>". number_format($Avancet2, 0, ",", ".")."</td>";

            echo "<td align=center>". number_format($Avancet3, 0, ",", ".") ."</td>";

            echo "<td align=center>". number_format($Avancet4, 0, ",", ".") ."</td>";

						echo "<td align=center>". number_format($Avance, 0, ",", ".") ."</td>";

            echo "<td align=center>". number_format($efectividad1, 2, ",", ".")."%</td>";

        echo "</tr>";
				echo "<td align=center colspan=10><div class=label label-success>".

					strtoupper($value->idUnidadMedida->descripcion)."	</div>";



				$etiquetas1=['I','II','III','IV'];
				$datos1=[$Avancet1,$Avancet2,$Avancet3,$Avancet4];
				$r1=mt_rand(0, 255);
				$g1=mt_rand(0, 255);
				$b1=mt_rand(0, 255);
				$Color[] = "rgba(".$r1.",".$g1.",".$b1.",0.5)";
				$bColor[] = "rgba(".$r1.",".$g1.",".$b1.",2)";
				$tip=mt_rand(1, 3);
				$values=[
						'1'=>'pie',
						'2'=>'doughnut',
						'3'=>'bar',
				];
				?>
<div class="col-xs-7 " align=center>
	<div class="center-block" align=center>


				<?= ChartJs::widget([
					//'type' => 'pie',
					'type' => $values[$tip],
					'options' => [
              //  'id' => 'cstat',

								//'responsive' => true,
     						//'animation'=> true,
        ],
					'clientOptions' => [
							'legend' => ['display' => true],
							'tooltips' => ['enabled' => true],
					],
					'data' => [
							'labels' => $etiquetas1,
							'datasets' => [
									[
										'backgroundColor' => $Color,
										'borderColor' => $bColor,
										'pointBackgroundColor' => $bColor,
										'pointBorderColor' => "#fff",
										'pointHoverBackgroundColor' => "#fff",
										'pointHoverBorderColor' => $bColor,
											'data' => $datos1,
									],

							],
					]
			]);
			?>
</div></div>
<?php
				echo "</td>";



		echo "</tr>";


      }

    ?>





    </tr>




</table>

</div>

<?php }
} ?>
