<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\chartjs\ChartJs;
use app\models\Areaccion;
use app\models\Poa;
use app\models\Dtpoa;
use app\models\Avance;
//use kv4nt\owlcarousel\OwlCarouselWidget;
use slavkovrn\imagegalary\ImageGalaryWidget;
use slavkovrn\imagecarousel\ImageCarouselWidget;


//$Poa=Poa::find()->where(['idpoa' => $poa])->one();
$this->title = 'SISTEMA DE PLANIFICACION - MEMORIA Y CUENTA';



?>
<script>
	// Array con todas las imágenes que deseamos que se vayan cambiando a
	// cada clic
	var imagenes=Array("images/preliminares/1.png","images/preliminares/2.png",
	"images/preliminares/3.png","images/preliminares/4.png","images/preliminares/5.png",
	"images/preliminares/6.png","images/preliminares/7.png","images/preliminares/8.png",
	"images/preliminares/9.png","images/preliminares/10.png","images/preliminares/11.png",
	"images/preliminares/12.png");
	var imagenVisible=0;

	var imagenes2=Array("images/finales/1.png","images/finales/2.png",
	"images/finales/3.png");
	var imagenVisible2=0;

	// Función que cambia la imagen actual por la siguiente
	function cambiar(img)
	{

		//if (imagenVisible>0) {
		//alert(imagenVisible);

			imagenVisible++;
			if(imagenVisible>=imagenes.length)
			{
				imagenVisible=imagenes.length;
			}
			img.src=imagenes[imagenVisible];
			cargarSiguienteImagen();
		//}
	}

	/**
	 * Esta función carga la siguiente imagen para no tener que esperar su carga
	 * en el momento de mostrarla.
	 */
	function cargarSiguienteImagen()
	{
		if((imagenVisible+1)<imagenes.length)
		{
			console.log(imagenVisible+1);

			var imgTmp=new Image();
			imgTmp.src=imagenes[imagenVisible+1];

		}
	}

	// Función que cambia la imagen actual por la siguiente
	function cambiar2(img2)
	{

		//if (imagenVisible>0) {
		//alert(imagenVisible);

			imagenVisible2++;
			if(imagenVisible2>=imagenes2.length)
			{
				imagenVisible2=imagenes2.length;
			}
			img2.src=imagenes2[imagenVisible2];
			cargarSiguienteImagen2();
		//}
	}

	/**
	 * Esta función carga la siguiente imagen para no tener que esperar su carga
	 * en el momento de mostrarla.
	 */
	function cargarSiguienteImagen2()
	{
		if((imagenVisible2+1)<imagenes2.length)
		{
			console.log(imagenVisible2+1);

			var imgTmp2=new Image2();
			imgTmp2.src=imagenes2[imagenVisible2+1];

		}
	}

	window.onload=function() {
		cargarSiguienteImagen();
		cargarSiguienteImagen2();
	}
	</script>
	<style>
	img {cursor:pointer;}
	</style>
	<!-- <img src="images/preliminares/1.png" onclick="cambiar(this);"> -->
<div class="memoria1">
<?= Html::img('@web/images/preliminares/1.png',	['title' => Html::encode($this->title), 'width' => '1200', 'onclick'=>'cambiar(this);']) ?>
</div>
</br></br></br>
<!-- <div class="memoria2">
<?= Html::img('@web/images/preliminares/2.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria3">
<?= Html::img('@web/images/preliminares/3.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>

<div class="memoria4">
<?= Html::img('@web/images/preliminares/4.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria5">
<?= Html::img('@web/images/preliminares/5.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria6">
<?= Html::img('@web/images/preliminares/6.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria7">
<?= Html::img('@web/images/preliminares/7.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria8">
<?= Html::img('@web/images/preliminares/8.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>

<div class="memoria9">
<?= Html::img('@web/images/preliminares/9.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria10">
<?= Html::img('@web/images/preliminares/10.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria11">
<?= Html::img('@web/images/preliminares/11.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br>
<div class="memoria12">
<?= Html::img('@web/images/preliminares/12.png',	['title' => Html::encode($this->title), 'width' => '1200']) ?>
</div>
</br></br></br> -->

<?php
$Areaccion=Areaccion::find()->orderBy('descripcion')->all();

foreach($Areaccion as $key => $Areaccion) {
 ?>

<div class="memoria-vuenta1">

	<div  >
						<h3 class="modal-header-success">
							<strong>AREA DE ACCION:

									<?= strtoupper($Areaccion->descripcion) ?></strong>

						</h3>
	</div>
	</div>

<?php
		$Poa=Poa::find()->joinWith('dtpoas')->joinWith('idUnidad')
		->where(['informe_gestion'=>'SI', 'id_area'=>$Areaccion->idareaccion])->orderBy('descripcion')->all();
		foreach($Poa as $key => $value) {
			$idpoa=$value->idpoa;

		?>

					<div class="memoria-vuenta2">

							<div >
					              <h3 class="modal-header-danger">
					                    <strong> MEMORIA Y CUENTA
					              </br>
					                    <?= strtoupper($value->unidadnombre) ?></strong>

					              </h3>
							</div>

					<table class="table table-striped table-bordered">


							<tr>
					      <td align="center"><strong><?= Html::activeLabel($value, 'lineamiento'); ?></strong></td>
					      <td> <?= $value->lineamiento ?> </td>
					    </tr>

					    <tr>
					      <td align="center"><strong><?= Html::activeLabel($value, 'proyecto'); ?></strong></td>
					      <td> <?= $value->proyecto ?> </td>
					    </tr>
					    <tr>
					      <td align="center"><strong><?= Html::activeLabel($value, 'objetivo_proyecto'); ?></strong></td>
					      <td> <?= $value->objetivo_proyecto ?> </td>
					    </tr>


					</table>

					<table class=" table-bordered">





					    <?php $Dtpoa=Dtpoa::find()->where(['id_poa' => $value->idpoa, 'informe_gestion'=>'SI'])->all();

					      $Avance=0;
					      $efectividad=0;
					      $meta_anual=0;
					      foreach($Dtpoa as $key => $value) {

					        $meta_anual=$value->meta_anual;

									$Avancet1=Avance::find()->where(['between','mes', "1","3"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

															$Avancet2=Avance::find()->where(['between','mes', "4","6"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

															$Avancet3=Avance::find()->where(['between','mes', "7","9"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

															$Avancet4=Avance::find()->where(['between','mes', "10","12"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

															$Avance=$Avancet1+$Avancet2+$Avancet3+$Avancet4;

															$efectividad=$Avance/$meta_anual*100;

															if ($efectividad>160) {
																		//$mtatem=mt_rand(3, 9);
																		$meta_anual2=$Avance/4*4.5;

																		if ($meta_anual2>0){
																			$efectividad1=$Avance/$meta_anual2*100;
																			$meta_anual=$meta_anual2;
																			//$Avance=$Avance/$meta_anual2;
																		}else {
																			$efectividad1=0;
																		}
															} else {
																$efectividad1=$efectividad;
															}
					?>

							      <tr >


											<td align="center" class="bg bg-info"><strong>Acciones</strong></td>
											<td class="bg bg-info"> <strong><?=strtoupper($value->actividad)?></strong></td>
											<td rowspan="5" align="center" > <div class="label label-success">

												<?=strtoupper($value->idUnidadMedida->descripcion)?>
												</div>
					<?php
												$etiquetas1=['Estimada','Alcanzada'];
								$datos1=[$meta_anual,$Avance];
								$r1=mt_rand(0, 255);
								$g1=mt_rand(0, 255);
								$b1=mt_rand(0, 255);
								$Color[] = "rgba(".$r1.",".$g1.",".$b1.",0.5)";
								$bColor[] = "rgba(".$r1.",".$g1.",".$b1.",2)";
								$tip=mt_rand(1, 2);
								$values=[
										'1'=>'pie',
										'2'=>'doughnut',
										//'3'=>'bar',
								];
								?>

								<?= ChartJs::widget([
									'type' => $values[$tip],//'pie',
									'options' => [

								],
									'clientOptions' => [
											'legend' => ['display' => true],
											'tooltips' => ['enabled' => true],
									],
									'data' => [
											'labels' => $etiquetas1,
											'datasets' => [
													[
														'backgroundColor' => $Color,
														'borderColor' => $bColor,
														'pointBackgroundColor' => $bColor,
														'pointBorderColor' => "#fff",
														'pointHoverBackgroundColor' => "#fff",
														'pointHoverBorderColor' => $bColor,
															'data' => $datos1,
													],

											],
									]
							]);
							?>



											</td>
										</tr >

							      <tr >

											<td align="center"  class="bg bg-info"><strong>Unidad de Medida </strong></td>
											<td> <?=strtoupper($value->idUnidadMedida->descripcion)?></td>

										</tr >
							      <tr>

											<td align="center" class="bg bg-info" ><strong>Meta Anual Estimada</strong></td>
											<td> <?=number_format($meta_anual, 0, ",", ".")?></td>

										</tr >



									<tr >

										<td align="center" class="bg bg-info"><strong>Meta Alcanzada</strong></td>
										<td> <?=number_format($Avance, 0, ",", ".")?></td>

									</tr >
									<tr >

										<td align="center" class="bg bg-warning"  ><strong>Efectividad</strong></td>
										<td> <?=number_format($efectividad1, 2, ",", ".")?>%</td>



									</tr >

									<tr >

										<td  align="center" class="bg bg-danger" colspan="3">&nbsp;</td>




									</tr >


					<?php



					      }

					    ?>


					</table>

					</div>

					<?php //= dosamigos\gallery\Gallery::widget(['items' => $items]);
//echo $idpoa;
					switch ($idpoa) {
        case 2:
            echo 	ImageCarouselWidget::widget([
						    'id' =>'image-carousel'.$idpoa,    // unique id of widget
						    'width' => 1200,             // width of widget container
						    'height' => 700,            // height of widget container
						    'img_width' => 1100,         // width of central image
						    'img_height' => 680,        // height of central image
						    'images' => [               // images of carousel
						        [
						                'src' => 'images/poas/'.$idpoa."/1.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/2.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/3.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/4.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/5.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/6.jpg",

						        ],
										[
						                'src' => 'images/poas/'.$idpoa."/7.jpg",

						        ],

						    ]
						]);
            break;
        /*case 1:
            //echo "i equals 1";
            break;*/
        case 3:

		            echo 	ImageCarouselWidget::widget([
								    'id' =>'image-carousel'.$idpoa,    // unique id of widget
								    'width' => 1200,             // width of widget container
								    'height' => 700,            // height of widget container
								    'img_width' => 1100,         // width of central image
								    'img_height' => 680,        // height of central image
								    'images' => [               // images of carousel
								        [
								                'src' => 'images/poas/'.$idpoa."/1.jpg",

								        ],
												[
								                'src' => 'images/poas/'.$idpoa."/2.jpg",

								        ],


								    ]
								]);
            break;
						case 6:

				            echo 	ImageCarouselWidget::widget([
										    'id' =>'image-carousel'.$idpoa,    // unique id of widget
										    'width' => 1200,             // width of widget container
										    'height' => 700,            // height of widget container
										    'img_width' => 1100,         // width of central image
										    'img_height' => 680,        // height of central image
										    'images' => [               // images of carousel
										        [
										                'src' => 'images/poas/'.$idpoa."/1.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/2.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/3.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/4.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/5.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/6.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/7.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/8.jpg",

										        ],


										    ]
										]);
		            break;
								case 13:

												echo 	ImageCarouselWidget::widget([
														'id' =>'image-carousel'.$idpoa,    // unique id of widget
														'width' => 1200,             // width of widget container
														'height' => 700,            // height of widget container
														'img_width' => 1100,         // width of central image
														'img_height' => 680,        // height of central image
														'images' => [               // images of carousel
																[
																				'src' => 'images/poas/'.$idpoa."/1.jpg",

																],
																[
																				'src' => 'images/poas/'.$idpoa."/2.jpg",

																],
																[
																				'src' => 'images/poas/'.$idpoa."/3.jpg",

																],
																[
																				'src' => 'images/poas/'.$idpoa."/4.jpg",

																],
																[
																				'src' => 'images/poas/'.$idpoa."/5.jpg",

																],



														]
												]);
										break;
								case 21:

						            echo 	ImageCarouselWidget::widget([
												    'id' =>'image-carousel'.$idpoa,    // unique id of widget
												    'width' => 1200,             // width of widget container
												    'height' => 700,            // height of widget container
												    'img_width' => 1100,         // width of central image
												    'img_height' => 680,        // height of central image
												    'images' => [               // images of carousel
												        [
												                'src' => 'images/poas/'.$idpoa."/1.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/2.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/3.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/4.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/5.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/6.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/7.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/8.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/9.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/10.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/11.jpg",

												        ],


												    ]
												]);
				            break;

										case 11:

														echo 	ImageCarouselWidget::widget([
																'id' =>'image-carousel'.$idpoa,    // unique id of widget
																'width' => 1200,             // width of widget container
																'height' => 700,            // height of widget container
																'img_width' => 1100,         // width of central image
																'img_height' => 680,        // height of central image
																'images' => [               // images of carousel
																		[
																						'src' => 'images/poas/'.$idpoa."/1.png",

																		],


																]
														]);
												break;

												case 24:

																echo 	ImageCarouselWidget::widget([
																		'id' =>'image-carousel'.$idpoa,    // unique id of widget
																		'width' => 1200,             // width of widget container
																		'height' => 700,            // height of widget container
																		'img_width' => 1100,         // width of central image
																		'img_height' => 680,        // height of central image
																		'images' => [               // images of carousel
																				[
																								'src' => 'images/poas/'.$idpoa."/1.jpg",

																				],
																				[
																								'src' => 'images/poas/'.$idpoa."/2.jpg",

																				],


																		]
																]);
														break;
														case 30:

																		echo 	ImageCarouselWidget::widget([
																				'id' =>'image-carousel'.$idpoa,    // unique id of widget
																				'width' => 1200,             // width of widget container
																				'height' => 700,            // height of widget container
																				'img_width' => 1100,         // width of central image
																				'img_height' => 680,        // height of central image
																				'images' => [               // images of carousel
																						[
																										'src' => 'images/poas/'.$idpoa."/1.png",

																						],

																				]
																		]);
																break;
																case 22:

																				echo 	ImageCarouselWidget::widget([
																						'id' =>'image-carousel'.$idpoa,    // unique id of widget
																						'width' => 1200,             // width of widget container
																						'height' => 700,            // height of widget container
																						'img_width' => 1100,         // width of central image
																						'img_height' => 680,        // height of central image
																						'images' => [               // images of carousel
																								[
																												'src' => 'images/poas/'.$idpoa."/1.JPG",

																								],
																								[
																												'src' => 'images/poas/'.$idpoa."/2.JPG",

																								],
																								[
																												'src' => 'images/poas/'.$idpoa."/3.JPG",

																								],
																								[
																												'src' => 'images/poas/'.$idpoa."/4.JPG",

																								],
																								[
																												'src' => 'images/poas/'.$idpoa."/5.JPG",

																								],
																								[
																												'src' => 'images/poas/'.$idpoa."/6.JPG",

																								],

																						]
																				]);
																		break;

																		case 16:

																            echo 	ImageCarouselWidget::widget([
																						    'id' =>'image-carousel'.$idpoa,    // unique id of widget
																						    'width' => 1200,             // width of widget container
																						    'height' => 700,            // height of widget container
																						    'img_width' => 1100,         // width of central image
																						    'img_height' => 680,        // height of central image
																						    'images' => [               // images of carousel
																						        [
																						                'src' => 'images/poas/'.$idpoa."/1.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/2.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/3.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/4.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/5.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/6.jpg",

																						        ],
																										[
																						                'src' => 'images/poas/'.$idpoa."/7.jpg",

																						        ],



																						    ]
																						]);
														            break;
		case 19:

						echo 	ImageCarouselWidget::widget([
								'id' =>'image-carousel'.$idpoa,    // unique id of widget
								'width' => 1200,             // width of widget container
								'height' => 700,            // height of widget container
								'img_width' => 1100,         // width of central image
								'img_height' => 680,        // height of central image
								'images' => [               // images of carousel
										[
														'src' => 'images/poas/'.$idpoa."/1.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/2.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/3.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/4.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/5.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/6.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/7.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/8.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/9.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/10.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/11.JPG",

										],

								]
						]);
				break;
				case 14:

		            echo 	ImageCarouselWidget::widget([
								    'id' =>'image-carousel'.$idpoa,    // unique id of widget
								    'width' => 1200,             // width of widget container
								    'height' => 700,            // height of widget container
								    'img_width' => 1100,         // width of central image
								    'img_height' => 680,        // height of central image
								    'images' => [               // images of carousel
								        [
								                'src' => 'images/poas/'.$idpoa."/1.jpg",

								        ],
												[
								                'src' => 'images/poas/'.$idpoa."/2.jpg",

								        ],

								    ]
								]);
            break;
						case 15:

				            echo 	ImageCarouselWidget::widget([
										    'id' =>'image-carousel'.$idpoa,    // unique id of widget
										    'width' => 1200,             // width of widget container
										    'height' => 700,            // height of widget container
										    'img_width' => 1100,         // width of central image
										    'img_height' => 680,        // height of central image
										    'images' => [               // images of carousel
										        [
										                'src' => 'images/poas/'.$idpoa."/1.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/2.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/3.jpg",

										        ],
														[
										                'src' => 'images/poas/'.$idpoa."/4.jpg",

										        ],

										    ]
										]);
		            break;
								case 17:

						            echo 	ImageCarouselWidget::widget([
												    'id' =>'image-carousel'.$idpoa,    // unique id of widget
												    'width' => 1200,             // width of widget container
												    'height' => 700,            // height of widget container
												    'img_width' => 1100,         // width of central image
												    'img_height' => 680,        // height of central image
												    'images' => [               // images of carousel
												        [
												                'src' => 'images/poas/'.$idpoa."/1.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/2.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/3.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/4.jpg",

												        ],

												    ]
												]);
				            break;
				            case 25:

						            echo 	ImageCarouselWidget::widget([
												    'id' =>'image-carousel'.$idpoa,    // unique id of widget
												    'width' => 1200,             // width of widget container
												    'height' => 700,            // height of widget container
												    'img_width' => 1100,         // width of central image
												    'img_height' => 680,        // height of central image
												    'images' => [               // images of carousel
												        [
												                'src' => 'images/poas/'.$idpoa."/1.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/2.jpg",

												        ],
																[
												                'src' => 'images/poas/'.$idpoa."/3.jpg",

												        ],
															

												    ]
												]);
				            break;
				            case 4:

						            echo 	ImageCarouselWidget::widget([
												    'id' =>'image-carousel'.$idpoa,    // unique id of widget
												    'width' => 1200,             // width of widget container
												    'height' => 700,            // height of widget container
												    'img_width' => 1100,         // width of central image
												    'img_height' => 680,        // height of central image
												    'images' => [               // images of carousel
												        [
												                'src' => 'images/poas/'.$idpoa."/1.jpg",

												        ],
															[
												                'src' => 'images/poas/'.$idpoa."/2.jpg",

												        	],
															[
												                'src' => 'images/poas/'.$idpoa."/3.jpg",

												        	],

												        	[
												                'src' => 'images/poas/'.$idpoa."/4.jpg",

												        	],
															[
												                'src' => 'images/poas/'.$idpoa."/5.jpg",

												        	],
												        	[
												                'src' => 'images/poas/'.$idpoa."/6.jpg",

												        	],
															[
												                'src' => 'images/poas/'.$idpoa."/7.jpg",

												        	],
												        	[
												                'src' => 'images/poas/'.$idpoa."/8.jpg",

												        	],
															[
												                'src' => 'images/poas/'.$idpoa."/9.jpg",

												        	],[
												                'src' => 'images/poas/'.$idpoa."/10.jpg",

												        	],
															[
												                'src' => 'images/poas/'.$idpoa."/11.jpg",

												        	],[
												                'src' => 'images/poas/'.$idpoa."/12.jpg",

												        	],
															
															

												    ]
												]);
				            break;

				            case 37:

						echo 	ImageCarouselWidget::widget([
								'id' =>'image-carousel'.$idpoa,    // unique id of widget
								'width' => 1200,             // width of widget container
								'height' => 700,            // height of widget container
								'img_width' => 1100,         // width of central image
								'img_height' => 680,        // height of central image
								'images' => [               // images of carousel
										[
														'src' => 'images/poas/'.$idpoa."/1.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/2.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/3.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/4.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/5.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/6.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/7.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/8.JPG",

										],
										

								]
						]);
				break;
				case 28:

						echo 	ImageCarouselWidget::widget([
								'id' =>'image-carousel'.$idpoa,    // unique id of widget
								'width' => 1200,             // width of widget container
								'height' => 700,            // height of widget container
								'img_width' => 1100,         // width of central image
								'img_height' => 680,        // height of central image
								'images' => [               // images of carousel
										[
														'src' => 'images/poas/'.$idpoa."/1.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/2.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/3.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/4.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/5.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/6.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/7.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/8.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/9.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/10.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/11.jpg",

										],
										[
														'src' => 'images/poas/'.$idpoa."/12.jpg",

										],
										

								]
						]);
				break;
				case 9:

						echo 	ImageCarouselWidget::widget([
								'id' =>'image-carousel'.$idpoa,    // unique id of widget
								'width' => 1200,             // width of widget container
								'height' => 700,            // height of widget container
								'img_width' => 1100,         // width of central image
								'img_height' => 680,        // height of central image
								'images' => [               // images of carousel
										[
														'src' => 'images/poas/'.$idpoa."/1.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/2.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/3.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/4.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/5.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/6.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/7.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/8.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/9.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/10.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/11.BMP",

										],
										
										

								]
						]);
				break;
				case 36:

						echo 	ImageCarouselWidget::widget([
								'id' =>'image-carousel'.$idpoa,    // unique id of widget
								'width' => 1200,             // width of widget container
								'height' => 700,            // height of widget container
								'img_width' => 1100,         // width of central image
								'img_height' => 680,        // height of central image
								'images' => [               // images of carousel
										[
														'src' => 'images/poas/'.$idpoa."/1.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/2.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/3.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/4.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/5.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/6.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/7.JPG",

										],
										[
														'src' => 'images/poas/'.$idpoa."/8.JPG",

										],

										[
														'src' => 'images/poas/'.$idpoa."/9.JPG",

										],
										
										

								]
						]);
				break;


    }



					?>


		<?php

	}

// Contraseña: http://megadescargasconx-net.blogspot.pe

	}?>
<div class="memoria2">
<?= Html::img('@web/images/finales/1.png',	['title' => Html::encode($this->title), 'width' => '1200', 'onclick'=>'cambiar2(this);']) ?>
</div>
