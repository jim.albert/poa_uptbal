<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Poa;
use app\models\Dtpoa;
/* @var $this yii\web\View */
/* @var $model app\models\Avance */
/* @var $form yii\widgets\ActiveForm */
if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $data=ArrayHelper::map(Poa::find()->joinWith('idUnidad')
  ->where(['id_usuario' => Yii::$app->user->identity->id])->orderBy('id_unidad ASC')->all(), 'idpoa','ano');
}else {
  $data=ArrayHelper::map(Poa::find()->orderBy('id_unidad ASC')->all(), 'idpoa','ano','idUnidadDesc');
}
?>

<div class="avance-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'id_dtpoa')->hiddenInput(['value'=>$actividadid])->label(false)

     ?>


     <?=  $form->field($model, 'mes')->widget(Select2::classname(), [
        'data' => ['1'=>'ENERO','2'=>'FEBRERO','3'=>'MARZO','4'=>'ABRIL',
        			'5'=>'MAYO','6'=>'JUNIO','7'=>'JULIO','8'=>'AGOSTO',
        			'9'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE',
        		],
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'valor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
