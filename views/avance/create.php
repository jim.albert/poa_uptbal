<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Avance */

$this->title = 'Registrar Avance';
$this->params['breadcrumbs'][] = ['label' => 'Avances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="avance-create">

   <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
