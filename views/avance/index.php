<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Dtpoa;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AvanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Avances';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  //$data=ArrayHelper::map(Poa::find()->joinWith('idUnidad')
  //->where(['id_usuario' => Yii::$app->user->identity->id])
  //->orderBy('id_unidad ASC')->all(), 'idpoa','ano');
  $data=ArrayHelper::map(Dtpoa::find()->joinWith('idPoa.idUnidad')
  ->where(['id_usuario' => Yii::$app->user->identity->id])
  ->orderBy('iddtpoa ASC')->all(), 'iddtpoa','actividad');
}else {
  //$data=ArrayHelper::map(Poa::find()->orderBy('id_unidad ASC')->all(), 'idpoa','ano','idUnidadDesc');
  $data=ArrayHelper::map(Dtpoa::find()->orderBy('iddtpoa ASC')->all(), 'iddtpoa','actividad','poadesc');
}

?>
<div class="avance-index">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Avance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idavance',
            //'idDtpoa.actividad',
            [
             'attribute' => 'id_dtpoa',
                'value'     => 'idDtpoa.actividad',
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_dtpoa',
                                'data' => $data,

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],
            //'mes',

            [
             'attribute' => 'mes',
                'value'     => function($searchModel){

                  $mes=['1'=>'ENERO','2'=>'FEBRERO','3'=>'MARZO','4'=>'ABRIL',
                    '5'=>'MAYO','6'=>'JUNIO','7'=>'JULIO','8'=>'AGOSTO',
                    '9'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE',];

                      return $mes[$searchModel->mes];
                },
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'mes',
                                'data' => ['1'=>'ENERO','2'=>'FEBRERO','3'=>'MARZO','4'=>'ABRIL',
                                '5'=>'MAYO','6'=>'JUNIO','7'=>'JULIO','8'=>'AGOSTO',
                                '9'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE',
                            ],

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],

            'valor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
