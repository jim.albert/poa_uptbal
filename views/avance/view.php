<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Avance */

$this->title = 'Identificador del Registro: '.$model->idavance;
$this->params['breadcrumbs'][] = ['label' => 'Avances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="avance-view">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idavance], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idavance], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idavance',
            //'id_dtpoa',
            'idDtpoa.actividad',
            //'idmes',

            [
             'attribute' => 'mes',
                'value'     => function($searchModel){

                  $mes=['1'=>'ENERO','2'=>'FEBRERO','3'=>'MARZO','4'=>'ABRIL',
                    '5'=>'MAYO','6'=>'JUNIO','7'=>'JULIO','8'=>'AGOSTO',
                    '9'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE',];

                      return $mes[$searchModel->mes];
                },
                'format'    => 'raw', // email, number
                //'contentOptions' => 
                
            ],

            'valor',
        ],
    ]) ?>

</div>
