<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Activar cuenta de Usuario para: ' . $model->nombre.' '.$model->apellido;

?>



<div class="usuario-update form">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
