<?php

//use Yii;
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

use app\models\Avance;
use app\models\Poa;
use app\models\Dtpoa;
use app\models\Unidadmedida;
use app\models\Unidadresponsable;
use app\models\Areaccion;

use app\models\Areaaccionunidadesponsable;

$this->title = 'PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO DE LA UPTBAL';



?>

    <div class="site-index">

        <div class="jumbotron">

            <table class="table table-striped">
                <tr>
                    <td colspan="6" class="danger text-danger">
                        <strong>   RESUMEN DE LA PLANIFICACION </strong>
                    </td>
                </tr>

                <tr class="text-danger">
                    <td  >
                        <strong>    UNIDAD RESPONSABLE</strong>
                    </td>
                    <td  >
                        <strong>    N° DE ACTIVIDADES </strong>
                    </td>
                    <td  >
                        <strong>    N° DE ACT. INFORME INSTITUCIONAL</strong>
                    </td>

                </tr>
                <?php

                  $Unidadresponsable = Unidadresponsable::find()->all();

                  foreach($Unidadresponsable as $key => $value) {

                    echo "<tr>";

                        echo "<td>". Html::a(strtoupper($value->descripcion), ['report/informef', 'id' => $value->idunidadresponsble], ['class' => 'label label-success', 'target'=>$value->idunidadresponsble])."</td>";


                        $Dtpoa = Dtpoa::find()->joinWith('idPoa.idUnidad')->where(['id_unidad' => $value->idunidadresponsble])->count();
                         echo "<td ><span class='badge'>". number_format($Dtpoa, 0, ",", ".") ."</span></td>";

                         $DtpoaMC = Dtpoa::find()->joinWith('idPoa.idUnidad')->where(['id_unidad' => $value->idunidadresponsble,'informe_gestion'=>'SI'])->count();
                         echo "<td ><span class='badge'>". number_format($DtpoaMC, 0, ",", ".") ."</span></td>";

                         // $meta_anual = Dtpoa::find()->joinWith('idPoa.idUnidad')->where(['id_unidad' => $value->idunidadresponsble])->SUM('meta_anual');
                         // echo "<td ><span class='badge'>". number_format($meta_anual, 0, ",", ".") ."</span></td>";
                         //
                         // $Avance = Avance::find()->joinWith('idDtpoa.idPoa.idUnidad')->where(['id_unidad' => $value->idunidadresponsble])->SUM('valor');
                         // echo "<td ><span class='badge'>". number_format($Avance, 0, ",", ".") ."</span></td>";
                         //
                         //
                         //    if ($Avance=="") {
                         //     $Avance="0";
                         //                       }
                         //    if ($meta_anual=="") {
                         //                         $meta_anual="0";
                         //                       }
                         //    if ($Avance!=0 AND $meta_anual!=0) {
                         //                         $efectividad=$Avance/$meta_anual*100;
                         //                       }else{
                         //      $efectividad="0";
                         //    }
                         //
                         //
                         // echo "<td ><span class='badge'>". number_format($efectividad, 2, ",", ".")."%</span></td>";


                    echo "</tr>";

                  }

                ?>



            </table>





        </div>

</div>
