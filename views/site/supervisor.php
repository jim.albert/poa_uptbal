<?php

//use Yii;
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

use app\models\Avance;
use app\models\Poa;
use app\models\Dtpoa;
use app\models\Unidadmedida;
use app\models\Unidadresponsable;
use app\models\Areaccion;

use app\models\Areaaccionunidadesponsable;

$this->title = 'PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO DE LA UPTBAL';

$Dependencia=Areaaccionunidadesponsable::find()->where(['id_usuario' => Yii::$app->user->identity->id])->all();

$jsc = <<< JS

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
JS;

$this->registerJs($jsc, $this::POS_END);

?>

    <div class="site-index">

        <div class="jumbotron">



<?php   foreach($Dependencia as $key => $Dependencia) {

              $Poa=Poa::find()->where(['id_unidad' => $Dependencia->id_unidadresponsble])->all();

  ?>
            <table class="table table-striped">
                <tr>
                    <td colspan="3" class="danger text-danger">
                        <strong>  <h4>

                        <?= Html::a(strtoupper($Dependencia->unidadnombre), ['report/informef', 'id' => $Dependencia->id_unidadresponsble],
                          ['class' => 'danger text-danger', 'target'=>$Dependencia->id_unidadresponsble,
                          'data-toggle' => 'tooltip',
                          'data-placement' => 'top',
                          'title' => 'CLICK PARA VER DETALLE DEL PLAN',])?></strong>
                          </h4>
                    </td>
                </tr>



                <?php  foreach($Poa as $key => $Poa) {

                  $Dtpoa=Dtpoa::find()->where(['id_poa' => $Poa->idpoa])->all();


                  ?>
                  <tr class="success text-success">
                      <td  >
                          <strong>   AÑO</strong>
                      </td>

                      <td  >
                          <strong>    PROYECTO </strong>
                      </td>
                      <td  >
                          <strong>    OBJETIVO DEL PROYECTO </strong>
                      </td>

                  </tr>
                  <tr >
                      <td  >
                          <strong>   <?= strtoupper($Poa->ano) ?></strong>
                      </td>

                      <td  >
                          <strong>    <?= strtoupper($Poa->proyecto) ?> </strong>
                      </td>
                      <td  >
                          <strong>    <?= strtoupper($Poa->objetivo_proyecto) ?> </strong>
                      </td>

                  </tr>



            <!-- </table>

            <table class="table table-striped">
                 -->
                 <tr>
                    <td colspan="3" class="success text-success">
                        <strong>  <h4>RESUMEN</h4> </strong>
                    </td>
                </tr>


                <tr >
                      <td colspan="3"  >
                          <table class="table table-striped">
                            <tr>
                             <td  class="text-success">
                                 <strong>  &nbsp; </strong>
                             </td><td  class="text-success">
                                 <strong>  ACCION </strong>
                             </td>
                             <td  class="text-success">
                                 <strong>    UNIDAD DE MEDIDA </strong>
                             </td>

                             <td class="text-success" >
                                 <strong>    META ANUAL </strong>
                             </td>
                             <td class="text-success" >
                                 <strong>    AVANCE </strong>
                             </td>
                             <td class="text-success" >
                                 <strong>    EFECTIVIDAD </strong>
                             </td>
                             </tr>
                             <?php  foreach($Dtpoa as $key => $Dtpoa) {

                               $meta_anual=$Dtpoa->meta_anual;

                               $Avance = Avance::find()->where(['id_dtpoa' => $Dtpoa->iddtpoa])
                               ->SUM('valor');

                               if ($Avance=="") {
                                $Avance="0";
                                                  }
                               if ($meta_anual=="") {
                                                    $meta_anual="0";
                                                  }
                               if ($Avance!=0 AND $meta_anual!=0) {
                                                    $efectividad=$Avance/$meta_anual*100;
                                                  }else{
                                 $efectividad="0";
                               }

                               ?>
           <tr class="text-default">
            <td  >

                  <?= Html::a('&nbsp;<strong><i class="glyphicon glyphicon-hand-up"></i></strong>&nbsp;',
                  ['avance/create2', 'iddtpoa' => $Dtpoa->iddtpoa],
                  ['class' => 'label label-warning',
                  'data-toggle' => 'tooltip',
                  'data-placement' => 'top',
                  'title' => 'REGISTRAR AVANCES',]) ?>

            </td>
            <td  >
                  <?= strtoupper($Dtpoa->actividad) ?>

            </td>
            <td  >
                  <?= strtoupper($Dtpoa->idUnidadMedida->descripcion) ?>



            </td>

            <td  >
                <span class="badge"><strong>   <?= number_format($meta_anual, 0, ",", ".") ?> </strong></span>
            </td>
            <td  >

                <span class="badge"><strong>     <?= number_format($Avance, 0, ",", ".") ?>
                </strong></span>
            </td>
            <td  >
              <span class="badge"><strong>     <?= number_format($efectividad, 0, ",", ".") ?>%
              </strong></span>
            </td>
            </tr>
                                <?php  }     ?>
                           </table>
                      </td>
               </tr>
             <?php  }
               ?>
            </table>
<?php  }
  ?>




        </div>

</div>
