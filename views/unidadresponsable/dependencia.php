<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unidadresponsable */

$this->title = 'Registrar Unidad Responsable';
/*$this->params['breadcrumbs'][] = ['label' => 'Unidades responsables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="unidadresponsable-create">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form_dependencia', [
        'model' => $model,'model2' => $model2,
    ]) ?>

</div>
