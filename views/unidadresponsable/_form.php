<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Unidadresponsable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unidadresponsable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'definicion')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'vision')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'mision')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'objetivo_general')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'objetivos_especificos')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'funciones')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'lineas_estrategicas')->textarea(['rows' => 6]) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
