<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Unidadresponsable */

$this->title = 'Identificador del Registro: '.$model->idunidadresponsble;
$this->params['breadcrumbs'][] = ['label' => 'Unidades responsables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unidadresponsable-view">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idunidadresponsble], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idunidadresponsble], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idunidadresponsble',
            'descripcion',
        ],
    ]) ?>

</div>
