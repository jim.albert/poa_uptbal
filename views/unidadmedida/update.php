<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unidadmedida */

$this->title = 'Actualizar Unidad de medida: ' . $model->idunidadmedida;
$this->params['breadcrumbs'][] = ['label' => 'Unidades de medidas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idunidadmedida, 'url' => ['view', 'id' => $model->idunidadmedida]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="unidadmedida-update">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
