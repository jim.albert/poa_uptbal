<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unidadmedida */

$this->title = 'Registrar Unidad de medida';
$this->params['breadcrumbs'][] = ['label' => 'Unidades de medidas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unidadmedida-create">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
