<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Poa;
use app\models\Unidadmedida;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DtpoaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Actividades';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $data=ArrayHelper::map(Poa::find()->joinWith('idUnidad')
  ->where(['id_usuario' => Yii::$app->user->identity->id])->orderBy('id_unidad ASC')->all(), 'idpoa','ano');

  $poa='idPoa.ano';

}else {
  $data=ArrayHelper::map(Poa::find()->orderBy('id_unidad ASC')->all(), 'idpoa','ano','idUnidadDesc');

  $poa='poadesc';
}
?>
<div class="dtpoa-index">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Actividad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'iddtpoa',
            //'id_poa',
            [
             'attribute' => 'id_poa',
                'value'     => $poa,
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_poa',
                                'data' => $data,

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],
            'actividad:ntext',
            //'id_unidad_medida',
            [
             'attribute' => 'id_unidad_medida',
                'value'     => 'idUnidadMedida.descripcion',
                'format'    => 'raw', // email, number
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'id_unidad_medida',
                                'data' => ArrayHelper::map(Unidadmedida::find()->orderBy('descripcion ASC')->all(), 'idunidadmedida','descripcion'),

                                'options' => [
                                    //'multiple' => true,
                                    'placeholder' => 'Seleccion una opción...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),

                ],
            //'enero',
            // 'febrero',
            // 'marzo',
            // 'abril',
            // 'mayo',
            // 'junio',
            // 'julio',
            // 'agosto',
            // 'setiembre',
            // 'octubre',
            // 'noviembre',
            // 'diciembre',
             'meta_anual',
            // 'informe_gestion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
