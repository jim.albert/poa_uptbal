<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Areaccion */

$this->title = 'Actualizar Area de Acción: ' . $model->idareaccion;
$this->params['breadcrumbs'][] = ['label' => 'Area de Acciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idareaccion, 'url' => ['view', 'id' => $model->idareaccion]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="areaccion-update">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
