<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Areaccion */

$this->title = 'Registrar Area de acción';
$this->params['breadcrumbs'][] = ['label' => 'Areas de acciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="areaccion-create">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
