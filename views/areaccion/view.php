<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Areaccion */

$this->title = 'Identificador del Registro: '.$model->idareaccion;
$this->params['breadcrumbs'][] = ['label' => 'Area de acciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="areaccion-view">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idareaccion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idareaccion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idareaccion',
            'descripcion',
        ],
    ]) ?>

</div>
