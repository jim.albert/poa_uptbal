<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "areaccion".
 *
 * @property integer $idareaccion
 * @property string $descripcion
 *
 * @property Poa[] $poas
 */
class Areaccion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areaccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion'], 'required'],
            [['descripcion'], 'string', 'max' => 200],
            [['descripcion'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idareaccion' => 'Id',
            'descripcion' => 'Area de Acción',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoas()
    {
        return $this->hasMany(Poa::className(), ['id_area' => 'idareaccion']);
    }

    public function getAreaaccionUnidadesponsables()
   {
       return $this->hasMany(Areaaccionunidadesponsable::className(), ['id_areaccion' => 'idareaccion']);
   }
}
