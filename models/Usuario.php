<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property integer $ci_tecnico
 * @property string $telefono
 * @property string $username
 * @property string $password
 * @property string $rol
 * @property string $estatus
 */
class Usuario extends \yii\db\ActiveRecord
{
  public $cedula;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'ci_tecnico', 'telefono', 'username', 'password', 'rol', 'estatus', 'cedula'], 'required'],
            [['ci_tecnico','cedula'], 'integer'],
            [['nombre', 'apellido'], 'string', 'max' => 50],
            [['telefono'], 'number', ],
            [['username', 'password', 'rol', 'estatus'], 'string', 'max' => 15],
            [['ci_tecnico'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'ci_tecnico' => 'Cédula de identidad',
            'telefono' => 'Teléfono',
            'username' => 'Usuario',
            'password' => 'Contraseña',
            'rol' => 'Tipo de Usuario',
            'estatus' => 'Estatus',
            'cedula' => 'Ingrese su Cédula de Identidad',
        ];
    }
}
