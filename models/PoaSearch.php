<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Poa;

/**
 * PoaSearch represents the model behind the search form about `app\models\Poa`.
 */
class PoaSearch extends Poa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idpoa', 'id_unidad'], 'integer'],
            [['ano', 'lineamiento', 'objetivo_esrategico', 'objetivo_parroquial', 'proyecto', 'objetivo_proyecto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Poa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idpoa' => $this->idpoa,
            'ano' => $this->ano,
            //'id_area' => $this->id_area,
            'id_unidad' => $this->id_unidad,
        ]);

        $query->andFilterWhere(['like', 'lineamiento', $this->lineamiento])
            ->andFilterWhere(['like', 'objetivo_esrategico', $this->objetivo_esrategico])
            ->andFilterWhere(['like', 'objetivo_parroquial', $this->objetivo_parroquial])
            ->andFilterWhere(['like', 'proyecto', $this->proyecto])
            ->andFilterWhere(['like', 'objetivo_proyecto', $this->objetivo_proyecto]);

        return $dataProvider;
    }
}
