<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dtpoa".
 *
 * @property integer $iddtpoa
 * @property integer $id_poa
 * @property string $actividad
 * @property integer $id_unidad_medida
 * @property integer $enero
 * @property integer $febrero
 * @property integer $marzo
 * @property integer $abril
 * @property integer $mayo
 * @property integer $junio
 * @property integer $julio
 * @property integer $agosto
 * @property integer $setiembre
 * @property integer $octubre
 * @property integer $noviembre
 * @property integer $diciembre
 * @property integer $meta_anual
 * @property string $informe_gestion
*
* @property Avance[] $avances
 * @property Poa $idPoa
 * @property Unidadmedida $idUnidadMedida
 */
class Dtpoa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dtpoa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_poa', 'actividad', 'id_unidad_medida', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'setiembre', 'octubre', 'noviembre', 'diciembre', 'meta_anual','informe_gestion'], 'required'],
            [['id_poa', 'id_unidad_medida', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'setiembre', 'octubre', 'noviembre', 'diciembre', 'meta_anual'], 'integer'],
            [['actividad'], 'string'],
            [['informe_gestion'], 'string', 'max' => 2],
            [['id_poa'], 'exist', 'skipOnError' => true, 'targetClass' => Poa::className(), 'targetAttribute' => ['id_poa' => 'idpoa']],
            [['id_unidad_medida'], 'exist', 'skipOnError' => true, 'targetClass' => Unidadmedida::className(), 'targetAttribute' => ['id_unidad_medida' => 'idunidadmedida']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iddtpoa' => 'Iddtpoa',
            'id_poa' => 'Plan Integral de Desarrollo',
            'actividad' => 'Acción',
            'id_unidad_medida' => 'Unidad de Medida',
            'enero' => 'Enero',
            'febrero' => 'Febrero',
            'marzo' => 'Marzo',
            'abril' => 'Abril',
            'mayo' => 'Mayo',
            'junio' => 'Junio',
            'julio' => 'Julio',
            'agosto' => 'Agosto',
            'setiembre' => 'Septiembre',
            'octubre' => 'Octubre',
            'noviembre' => 'Noviembre',
            'diciembre' => 'Diciembre',
            'meta_anual' => 'Meta Anual',
            'informe_gestion' => 'Pertenece al PIDIPA UPTBAL',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPoa()
    {
        return $this->hasOne(Poa::className(), ['idpoa' => 'id_poa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnidadMedida()
    {
        return $this->hasOne(Unidadmedida::className(), ['idunidadmedida' => 'id_unidad_medida']);
    }
    public function getAvances()
   {
       return $this->hasMany(Avance::className(), ['id_dtpoa' => 'iddtpoa']);
   }

   public function getPoadesc()
  {
      return $this->idPoa->idUnidadDesc." - ".$this->idPoa->ano;
  }
}
