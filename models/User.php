<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    
    public $id;
    public $nombre;
    public $apellido;
    public $ci_tecnico;
    public $telefono;
    public $username;
    public $password;
    public $rol;
    public $estatus;
    public $authKey;
    public $accessToken;

   /* private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'censo',
            'password' => 'clap',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        $user = Usuario::find()
                ->where("estatus=:estatus", [":estatus" => "ACTIVO"])
                ->andWhere("id=:id", ["id" => $id])
                ->one();
        
        return isset($user) ? new static($user) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;*/
        $users = Usuario::find()
                ->where("estatus=:estatus", [":estatus" => "ACTIVO"])
                ->andWhere("password=:password", [":password" => $password])
                ->all();
        
        foreach ($users as $user) {
            if ($user->password === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
        $users = Usuario::find()
                ->where("estatus=:estatus", [":estatus" => "ACTIVO"])
                ->andWhere("username=:username", [":username" => $username])
                ->all();
        
        foreach ($users as $user) {
            if (strcasecmp($user->username, $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //return $this->password === $password;
        if (crypt($password, $this->password) == $this->password)
        {
        return $password === $password;
        }
    }

    public static function isUserAdmin($id)
    {
       if (Usuario::findOne(['id' => $id, 'estatus' => 'ACTIVO', 'rol' => 'ADMINISTRADOR'])){
        return true;
       } else {

        return false;
       }

    }
    public static function isUserTecnico($id)
    {
       if (Usuario::findOne(['id' => $id, 'estatus' => 'ACTIVO', 'rol' => 'TECNICO'])){
        return true;
       } else {

        return false;
       }

    }
    public static function isUserSup($id)
    {
       if (Usuario::findOne(['id' => $id, 'estatus' => 'ACTIVO', 'rol' => 'SUPERVISOR'])){
        return true;
       } else {

        return false;
       }

    }
    public static function isUserDistribuidor($id)
    {
       if (Usuario::findOne(['id' => $id, 'estatus' => 'ACTIVO', 'rol' => 'DISTRIBUIDOR'])){
        return true;
       } else {

        return false;
       }

    }
}
