<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "avance".
 *
 * @property integer $idavance
 * @property integer $id_dtpoa
 * @property integer $mes
 * @property integer $valor
 *
 * @property Dtpoa $idDtpoa
 */
class Avance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $poa;
    public $trimestre;
    public static function tableName()
    {
        return 'avance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dtpoa', 'mes', 'valor'], 'required'],
            [['id_dtpoa', 'mes', 'valor'], 'number'],
            [['id_dtpoa'], 'exist', 'skipOnError' => true, 'targetClass' => Dtpoa::className(), 'targetAttribute' => ['id_dtpoa' => 'iddtpoa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idavance' => 'Idavance',
            'id_dtpoa' => 'Actividad',
            'mes' => 'Mes',
            'valor' => 'Valor',
            'trimestre' => 'Trimestre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDtpoa()
    {
        return $this->hasOne(Dtpoa::className(), ['iddtpoa' => 'id_dtpoa']);
    }

    
   
}
