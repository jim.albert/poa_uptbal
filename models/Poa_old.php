<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poa".
 *
 * @property integer $idpoa
 * @property string $ano
 * @property string $lineamiento
 * @property string $objetivo_esrategico
 * @property string $objetivo_parroquial
 * @property string $proyecto
 * @property string $objetivo_proyecto
 * @property integer $id_unidad
 *
 * @property Dtpoa[] $dtpoas

 * @property AreaaccionUnidadesponsable $unidad
 */
class Poa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ano', 'lineamiento', 'objetivo_esrategico', 'id_unidad'], 'required'],
            [['ano'], 'safe'],
            [['ano'], 'validaPoa'],
            [['lineamiento', 'objetivo_esrategico', 'objetivo_parroquial', 'proyecto','objetivo_proyecto'], 'string'],
            [['id_unidad'], 'integer'],
            [['id_unidad'], 'exist', 'skipOnError' => true, 'targetClass' => Areaaccionunidadesponsable::className(), 'targetAttribute' => ['id_unidad' => 'idareaaccionunidadesponsable']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpoa' => 'Id',
            'ano' => 'Año',
            'lineamiento' => 'Lineamiento Estratégico Segundo Plan Socialista de Desarrollo Económico y Social de la Nación 2013 - 2019',
            'objetivo_esrategico' => 'Objetivo Estratégico Plan de Desarrollo Territorial',
            'objetivo_parroquial' => 'Objetivo Territorial',
            'proyecto' => 'Proyecto POA',
            'objetivo_proyecto' => 'Objetivo del Proyecto',
            'id_unidad' => 'Unidad Responsable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDtpoas()
    {
        return $this->hasMany(Dtpoa::className(), ['id_poa' => 'idpoa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnidad()
    {
        return $this->hasOne(Areaaccionunidadesponsable::className(), ['idareaaccionunidadesponsable' => 'id_unidad']);
    }

    public function getIdUnidadDesc()
    {
        return $this->idUnidad->unidadnombre;
    }


    /*public function getUnidadnombre()
    {
        //return $this->hasOne(Unidadresponsable::className(), ['idunidadresponsble' => 'id_unidad']);
        return $this->idUnidad->descripcion." - ".$this->ano;
    }*/

    public function validaPoa($attribute, $params, $validator)
     {
         //if (!in_array($this->$attribute, ['1', '2'])) {
         $ceddt = Poa::find()->where(['id_unidad' => $this->id_unidad,'ano'=>$this->ano])->count();

         if ($ceddt>=1) {

             $datos = Poa::find()->where(['id_unidad' => $this->id_unidad,'ano'=>$this->ano])->one();

             $this->addError($attribute, 'Ya posee un Plan registrado para el año: '.$this->ano);
         }

     }
}
