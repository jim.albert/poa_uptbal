<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dtpoa;

/**
 * DtpoaSearch represents the model behind the search form about `app\models\Dtpoa`.
 */
class DtpoaSearch extends Dtpoa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iddtpoa', 'id_poa', 'id_unidad_medida', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'setiembre', 'octubre', 'noviembre', 'diciembre', 'meta_anual'], 'integer'],
            [['actividad', 'informe_gestion'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dtpoa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iddtpoa' => $this->iddtpoa,
            'id_poa' => $this->id_poa,
            'id_unidad_medida' => $this->id_unidad_medida,
            'enero' => $this->enero,
            'febrero' => $this->febrero,
            'marzo' => $this->marzo,
            'abril' => $this->abril,
            'mayo' => $this->mayo,
            'junio' => $this->junio,
            'julio' => $this->julio,
            'agosto' => $this->agosto,
            'setiembre' => $this->setiembre,
            'octubre' => $this->octubre,
            'noviembre' => $this->noviembre,
            'diciembre' => $this->diciembre,
            'meta_anual' => $this->meta_anual,
        ]);

        if (Yii::$app->user->identity->rol=="SUPERVISOR") {
          $query->joinWith('idPoa.idUnidad')
          ->where(['id_usuario' => Yii::$app->user->identity->id]);

        }

       $query->andFilterWhere(['like', 'actividad', $this->actividad])
           ->andFilterWhere(['like', 'informe_gestion', $this->informe_gestion]);


        return $dataProvider;
    }
}
