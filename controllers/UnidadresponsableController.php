<?php

namespace app\controllers;

use Yii;
use app\models\Unidadresponsable;
use app\models\UnidadresponsableSearch;

use app\models\Areaaccionunidadesponsable;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UnidadresponsableController implements the CRUD actions for Unidadresponsable model.
 */
class UnidadresponsableController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Unidadresponsable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnidadresponsableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Unidadresponsable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Unidadresponsable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Unidadresponsable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->idunidadresponsble]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionRegdependencia()
    {
        $model = new Unidadresponsable();
        $model2 = new Areaaccionunidadesponsable();

        if ($model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) ) {

          if ($model->save() ) {
            $model2->id_usuario=Yii::$app->user->identity->id;
            $model2->id_unidadresponsble=$model->idunidadresponsble;

            if ($model2->save()) {
              return $this->redirect(['/site/index', ]);
            }else {
              $errores2 = "";

              foreach ( $model2->getErrors() as $key => $value ) {
                  foreach ( $value as $row => $field ) {
                      $errores2 .= $field . "<br>";
                  }
              }
              Yii::$app->session->setFlash($errores2);
              throw new NotFoundHttpException($errores2);
            }

          }else {
            $errores = "";

            foreach ( $model->getErrors() as $key => $value ) {
                foreach ( $value as $row => $field ) {
                    $errores .= $field . "<br>";
                }
            }

            Yii::$app->session->setFlash('error',$errores);
            throw new NotFoundHttpException($errores);
          }


        } else {
            return $this->render('dependencia', [
                'model' => $model, 'model2' => $model2,
            ]);
        }
    }
    /**
     * Updates an existing Unidadresponsable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->idunidadresponsble]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate2()
    {
      $Dependencia=Areaaccionunidadesponsable::find()->where(['id_usuario' => Yii::$app->user->identity->id])->one();

        $model = $this->findModel($Dependencia->id_unidadresponsble);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/site/index', 'id' => $model->idunidadresponsble]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Unidadresponsable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Unidadresponsable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unidadresponsable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unidadresponsable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
