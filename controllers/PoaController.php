<?php

namespace app\controllers;

use Yii;
use app\models\Poa;
use app\models\PoaSearch;
use app\models\PoaSearchsupervisor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PoaController implements the CRUD actions for Poa model.
 */
class PoaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PoaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex2()
    {
        $searchModel = new PoaSearchsupervisor();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Poa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Poa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (Yii::$app->user->identity->rol=="SUPERVISOR") {
              return $this->redirect(['index2', 'id' => $model->idpoa]);
            }else {
              return $this->redirect(['index', 'id' => $model->idpoa]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionRegistro()
    {
        $model = new Poa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index2', 'id' => $model->idpoa]);
        } else {
            return $this->render('registro', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Poa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['index', 'id' => $model->idpoa]);
            if (Yii::$app->user->identity->rol=="SUPERVISOR") {
              return $this->redirect(['index2', 'id' => $model->idpoa]);
            }else {
              return $this->redirect(['index', 'id' => $model->idpoa]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Poa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        //return $this->redirect(['index']);
        if (Yii::$app->user->identity->rol=="SUPERVISOR") {
          return $this->redirect(['index2']);
        }else {
          return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Poa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
